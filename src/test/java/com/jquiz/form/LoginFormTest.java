package com.jquiz.form;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LoginFormTest {

    /** form common */

    @Test
    public void emptyForm() {
        com.jquiz.form.LoginForm form = com.jquiz.form.LoginForm.emptyForm();
        form.validate();

        assertTrue(form.hasErrors());
        assertTrue(form.hasError("username"));
        assertTrue(form.hasError("password"));
    }

    @Test
    public void formOk() {
        com.jquiz.form.LoginForm form = com.jquiz.form.LoginForm.emptyForm();
        form.setValue("username", "username");
        form.setValue("password", "password");
        form.validate();

        assertFalse(form.hasErrors());
    }

    /** username */

    @Test
    public void usernameOk() {
        com.jquiz.form.LoginForm form = com.jquiz.form.LoginForm.emptyForm();
        form.setValue("username", "username");
        form.validate();

        assertFalse(form.hasError("username"));
    }

    @Test
    public void usernameEmpty() {
        com.jquiz.form.LoginForm form = com.jquiz.form.LoginForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("username"));
        assertEquals(form.getError("username"), "Required");
    }

    @Test
    public void usernameIncorrectFormat() {
        com.jquiz.form.LoginForm form = com.jquiz.form.LoginForm.emptyForm();
        form.setValue("username", "!@#$%^");
        form.validate();

        assertTrue(form.hasError("username"));
        assertEquals(form.getError("username"), "Incorrect format");
    }

    @Test
    public void usernameIncorrectLength() {
        com.jquiz.form.LoginForm form = com.jquiz.form.LoginForm.emptyForm();
        form.setValue("username", "qwertyuiopqwertyuiopqwertyuiopqwertyuiop");
        form.validate();

        assertTrue(form.hasError("username"));
        assertEquals(form.getError("username"), "Length is too big");
    }

    /** password */

    @Test
    public void passwordOk() {
        com.jquiz.form.LoginForm form = com.jquiz.form.LoginForm.emptyForm();
        form.setValue("password", "password");
        form.validate();

        assertFalse(form.hasError("password"));
    }

    @Test
    public void passwordEmpty() {
        com.jquiz.form.LoginForm form = com.jquiz.form.LoginForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("password"));
        assertEquals(form.getError("password"), "Required");
    }

    @Test
    public void passwordsIncorrectLength() {
        com.jquiz.form.LoginForm form = com.jquiz.form.LoginForm.emptyForm();
        form.setValue("password", "qwertyuiopqwertyuiopqwertyuiopqwertyuiop");
        form.validate();

        assertTrue(form.hasError("password"));
        assertEquals(form.getError("password"), "Length is too big");
    }

}