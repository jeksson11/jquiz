--
-- Структура таблицы `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` enum('Regular','Editor') COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Структура таблицы `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `correct_answers_percent` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `question_text` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `quiz_id` INT NOT NULL,
  PRIMARY KEY (`id`, `quiz_id`),
  INDEX `fk_question_test_idx` (`quiz_id` ASC),
  CONSTRAINT `fk_question_test`
    FOREIGN KEY (`quiz_id`)
    REFERENCES `jquiz`.`quiz` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `answer_text` TEXT COLLATE utf8_unicode_ci NOT NULL,
  `is_correct_answer` ENUM('yes','no') COLLATE utf8_unicode_ci NOT NULL,
  `question_id` INT NOT NULL,
  PRIMARY KEY (`id`, `question_id`),
  INDEX `fk_answer_question1_idx` (`question_id` ASC),
  CONSTRAINT `fk_answer_question1`
    FOREIGN KEY (`question_id`)
    REFERENCES `jquiz`.`question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `member_answer`
--

CREATE TABLE IF NOT EXISTS `member_answer` (
  `member_id` INT NOT NULL,
  `answer_id` INT NOT NULL,
  `answer` ENUM('yes','no') NOT NULL,
  PRIMARY KEY (`member_id`, `answer_id`),
  INDEX `fk_member_answer_2_idx` (`answer_id` ASC),
  CONSTRAINT `fk_member_answer_1`
    FOREIGN KEY (`member_id`)
    REFERENCES `jquiz`.`member` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_member_answer_2`
    FOREIGN KEY (`answer_id`)
    REFERENCES `jquiz`.`answer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `member`
--

INSERT INTO `member` (`id`, `role`, `username`, `email`, `firstName`, `lastName`, `password`) VALUES
  (1, 'Editor', 'quizmaster', 'quizmaster@jquiz.com', 'Master', 'Quiz', '06405446f73ec9a2d6dc70f1289a18ba'),
  (2, 'Regular', 'user01', 'user01@jquiz.com', 'Test', 'User', 'b75705d7e35e7014521a46b532236ec3');

--
-- Дамп данных таблицы `quiz`
--

INSERT INTO jquiz.quiz (id, name, description, correct_answers_percent) VALUES (1, 'Methods', 'This quiz contains questions about methods', 70);
INSERT INTO jquiz.quiz (id, name, description, correct_answers_percent) VALUES (2, 'Basic', 'This quiz contains basic Java questions', 70);
INSERT INTO jquiz.quiz (id, name, description, correct_answers_percent) VALUES (3, 'OOP', 'This quiz contains questions about Object-Oriented Programming', 70);

--
-- Дамп данных таблицы `question`
--

INSERT INTO jquiz.question (id, question_text, quiz_id) VALUES (1, 'Suppose your method does not return any value, which of the following keywords can be used as a return type?', 1);
INSERT INTO jquiz.question (id, question_text, quiz_id) VALUES (2, 'Which of the following is not a Java keyword?', 2);
INSERT INTO jquiz.question (id, question_text, quiz_id) VALUES (3, 'Choose the appropriate data type for this value: 5.5', 2);
INSERT INTO jquiz.question (id, question_text, quiz_id) VALUES (4, 'What is the correct syntax for java main method?', 2);
INSERT INTO jquiz.question (id, question_text, quiz_id) VALUES (5, 'One of the feature of Object-based programming language is Inheritance.', 3);
INSERT INTO jquiz.question (id, question_text, quiz_id) VALUES (6, 'Name should be started with lowercase letter in case of ______ .', 3);
INSERT INTO jquiz.question (id, question_text, quiz_id) VALUES (7, 'Abstract method can be in _______ .', 3);

--
-- Дамп данных таблицы `answer`
--

INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (1, 'void', 'yes', 1);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (2, 'int', 'no', 1);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (3, 'double', 'no', 1);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (4, 'public', 'no', 1);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (5, 'None of the above', 'no', 1);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (6, 'static', 'no', 2);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (7, 'try', 'no', 2);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (8, 'Integer', 'yes', 2);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (9, 'new', 'no', 2);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (10, 'int', 'no', 3);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (11, 'double', 'yes', 3);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (12, 'boolean', 'no', 3);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (13, 'String', 'no', 3);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (14, 'public void main(String[] args)', 'no', 4);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (15, 'public static void main(string[] args)', 'no', 4);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (16, 'public static void main()', 'no', 4);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (17, 'none of the above', 'yes', 4);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (18, 'False', 'no', 5);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (19, 'True', 'yes', 5);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (20, 'Interface', 'no', 6);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (21, 'method', 'yes', 6);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (22, 'Normal class', 'no', 7);
INSERT INTO jquiz.answer (id, answer_text, is_correct_answer, question_id) VALUES (23, 'abstract class', 'yes', 7);