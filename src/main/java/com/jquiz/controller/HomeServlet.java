package com.jquiz.controller;

import com.jquiz.model.Quiz;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("page_title", "Home");

        ArrayList<Quiz> lastQuizzesList = new ArrayList<>();
        HashMap<String,String> errorsMap = new HashMap<>();
        try {
            lastQuizzesList = Quiz.getLast();
        } catch (SQLException e) {
            errorsMap.put("SQL Error", "Error occurred during quizzes selection!");
        }
        request.setAttribute("lastQuizzesList", lastQuizzesList);
        request.setAttribute("errorsMap", errorsMap);
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

}
