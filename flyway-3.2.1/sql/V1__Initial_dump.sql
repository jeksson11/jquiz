--
-- Структура таблицы `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` enum('Regular','Editor') COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `quiz` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `correct_answers_percent` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `question` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `question_text` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `quiz_id` INT NOT NULL,
  PRIMARY KEY (`id`, `quiz_id`),
  INDEX `fk_question_test_idx` (`quiz_id` ASC),
  CONSTRAINT `fk_question_test`
    FOREIGN KEY (`quiz_id`)
    REFERENCES `jquiz`.`quiz` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `answer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `answer_text` TEXT COLLATE utf8_unicode_ci NOT NULL,
  `is_correct_answer` ENUM('yes','no') COLLATE utf8_unicode_ci NOT NULL,
  `question_id` INT NOT NULL,
  PRIMARY KEY (`id`, `question_id`),
  INDEX `fk_answer_question1_idx` (`question_id` ASC),
  CONSTRAINT `fk_answer_question1`
    FOREIGN KEY (`question_id`)
    REFERENCES `jquiz`.`question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `member_answer` (
  `member_id` INT NOT NULL,
  `answer_id` INT NOT NULL,
  `answer` ENUM('yes','no') NOT NULL,
  PRIMARY KEY (`member_id`, `answer_id`),
  INDEX `fk_member_answer_2_idx` (`answer_id` ASC),
  CONSTRAINT `fk_member_answer_1`
    FOREIGN KEY (`member_id`)
    REFERENCES `jquiz`.`member` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_member_answer_2`
    FOREIGN KEY (`answer_id`)
    REFERENCES `jquiz`.`answer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
--
-- Дамп данных таблицы `member`
--

INSERT INTO `member` (`id`, `role`, `username`, `email`, `firstName`, `lastName`, `password`) VALUES
  (1, 'Editor', 'quizmaster', 'quizmaster@jquiz.com', 'Master', 'Quiz', '06405446f73ec9a2d6dc70f1289a18ba'),
  (2, 'Regular', 'user01', 'user01@jquiz.com', 'Test', 'User', 'b75705d7e35e7014521a46b532236ec3');

INSERT INTO `quiz` VALUES (1,'Methods','This quiz contains questions about methods',70);

INSERT INTO `question` VALUES (1,'Suppose your method does not return any value, which of the following keywords can be used as a return type?',1);

INSERT INTO `answer` VALUES
  (1,'void','yes',1),
  (2,'int','no',1),
  (3,'double','no',1),
  (4,'public','no',1),
  (5,'None of the above','no',1);
